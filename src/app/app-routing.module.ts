import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';


const routes: Routes = [
  {
    path: 'films', loadChildren: () => import('./films/films.module')
      .then(m => m.FilmsModule)
  },

  {
    path: 'series', loadChildren: () => import('./series/series.module')
      .then(m => m.SeriesModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
