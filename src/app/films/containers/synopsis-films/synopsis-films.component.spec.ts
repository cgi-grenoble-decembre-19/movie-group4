import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SynopsisFilmsComponent } from './synopsis-films.component';

describe('SynopsisFilmsComponent', () => {
  let component: SynopsisFilmsComponent;
  let fixture: ComponentFixture<SynopsisFilmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SynopsisFilmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SynopsisFilmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
