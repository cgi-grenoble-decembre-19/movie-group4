import { FilmService } from './../../services/film.service';
import { Film } from './../../../models/film';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-films',
  templateUrl: './list-films.component.html',
  styleUrls: ['./list-films.component.scss']
})
export class ListFilmsComponent implements OnInit {
  films: Film[];
  filmHeader = [ 'Titre Original', 'Popularité', 'Vote' ];

  constructor(
    private filmService: FilmService,
    private router: Router) { }

  ngOnInit() {
    this.filmService.list().subscribe(
      (data) => this.films = data
    );
  }

  goToSynopsis(id: number){
    this.router.navigate(['/films', 'synopsis', id]);
  }

}
