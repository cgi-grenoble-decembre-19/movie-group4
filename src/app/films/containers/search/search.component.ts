import { Component, OnInit } from '@angular/core';
import { Film } from 'src/app/models/film';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  constructor() { }

  searchText: string;
  name = 'TempName';
  films: Film[];

  ngOnInit() {

  }
/*
  Search() {
    this.course = this.course.filter(res=>{
      return res.name.toLocaleLowerCase().match(this.name.toLocaleLowerCase)
    });
  }
*/
}
