import { SharedModule } from './../shared/shared.module';
import { FilmsRoutingModule } from './films-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListFilmsComponent } from './containers/list-films/list-films.component';
import { SynopsisFilmsComponent } from './containers/synopsis-films/synopsis-films.component';



@NgModule({
  declarations: [ListFilmsComponent, SynopsisFilmsComponent],
  imports: [
    CommonModule,
    FilmsRoutingModule,
    SharedModule
  ]
})
export class FilmsModule { }
