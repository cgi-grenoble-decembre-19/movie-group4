import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, map } from 'rxjs/operators';
import { Film } from 'src/app/models/film';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  constructor(private http: HttpClient) { }

  // List des films les plus populaires
  list() {
    return this.http.get<Film[]>(`${environment.urlApi}discover/movie` +
    `?api_key=${environment.apiKey}&language=fr&region=FR&sort_by=popularity.descmovie`).pipe(
      tap(
        (data: any) => console.log(data.results)
      ),
      map((data: any) => data.results.map( (film) => {
        const f: Film = new Film(film);
        console.log(f);
        return f;
      })),
      tap(
        (data: any) => console.log(data)
      )
    );
  }

  // Détails d'un film
  get(id: string) {
    return this.http.get<Film>(`${environment.urlApi}movie/${id}?api_key=${environment.apiKey}`).pipe(
      map((film) => new Film(film))
    );
  }

  // List des films recommandés par rapport à un film
  getRecommandation(id: string) {
    return this.http.get<Film[]>(`${environment.urlApi}movie/${id}/similar?api_key=${environment.apiKey}`).pipe(
      map((data) => data.map(film => new Film(film)))
    );
  }
}
