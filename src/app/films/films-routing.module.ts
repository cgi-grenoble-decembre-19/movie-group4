import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListFilmsComponent } from './containers/list-films/list-films.component';
import { SynopsisFilmsComponent } from './containers/synopsis-films/synopsis-films.component';


const routes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {path: 'list', component: ListFilmsComponent},
  {path: 'synopsis/:id', component: SynopsisFilmsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FilmsRoutingModule { }
