import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SynopsisSeriesComponent } from './synopsis-series.component';

describe('SynopsisSeriesComponent', () => {
  let component: SynopsisSeriesComponent;
  let fixture: ComponentFixture<SynopsisSeriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SynopsisSeriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SynopsisSeriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
