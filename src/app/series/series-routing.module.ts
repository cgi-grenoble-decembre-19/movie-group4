import { SynopsisSeriesComponent } from './containers/synopsis-series/synopsis-series.component';
import { ListSeriesComponent } from './containers/list-series/list-series.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {path: 'list', component: ListSeriesComponent},
  {path: 'synopsis', component: SynopsisSeriesComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeriesRoutingModule { }
