import { SeriesRoutingModule } from './series-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListSeriesComponent } from './containers/list-series/list-series.component';
import { SynopsisSeriesComponent } from './containers/synopsis-series/synopsis-series.component';



@NgModule({
  declarations: [ListSeriesComponent, SynopsisSeriesComponent],
  imports: [
    CommonModule,
    SeriesRoutingModule
  ]
})
export class SeriesModule { }
