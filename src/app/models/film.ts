export class Film {
    id: number;
    title: string;
    langue?: string;
    overview?: string;
    popularity?: number;
    vote_average?: number;

    constructor(fields?: Partial<Film>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
