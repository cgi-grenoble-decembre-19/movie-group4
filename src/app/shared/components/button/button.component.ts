import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

@Input() btnTxt: string;
@Output() outBtn: EventEmitter<void> = new EventEmitter();

  btnClick(event) {
    this.outBtn.emit();
  }
  constructor() { }

  ngOnInit() {
  }

}
