import { SharedModule } from './shared/shared.module';
import { SeriesModule } from './series/series.module';
import { FilmsModule } from './films/films.module';
import { UiModule } from './ui/ui.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';
import { SearchComponent } from './films/containers/search/search.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    AppComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule,
    UiModule,
    FilmsModule,
    SeriesModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [SearchComponent]
})
export class AppModule {

  constructor(router: Router) {
    const replacer = (key, value) => (typeof value === 'function') ? value.name : value;
    console.log('Routes : ', JSON.stringify(router.config, replacer, 2));
  }
}
