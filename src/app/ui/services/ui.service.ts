import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UiService {
  private openP$ = new BehaviorSubject(true);
  public open$ = this.openP$.asObservable();

  constructor() { }

  toggle() {
    const val = !this.openP$.value;
    this.openP$.next(val);
  }
}
