import { NavComponent } from './../nav/nav.component';
import { Component, OnInit } from '@angular/core';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  open = false;
  textButton = 'Close Menu';
  versionValue: number;

  constructor() { }

  ngOnInit() {
  }

  toggle() {
    this.open = !this.open;
    this.open ? this.textButton = 'Open Menu' : this.textButton = 'Close Menu';
  }

}
