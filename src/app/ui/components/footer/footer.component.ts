import { filter } from 'rxjs/operators';
import { Component, OnInit, VERSION, } from '@angular/core';
import * as day from 'dayjs';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  angularVersion = VERSION.full;
  currentDate = day();

  constructor() {
  }

  ngOnInit() {
  }

}
