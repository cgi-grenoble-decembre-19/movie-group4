import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './components/nav/nav.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { UiComponent } from './containers/ui/ui.component';



@NgModule({
  declarations: [NavComponent, FooterComponent, HeaderComponent, UiComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    UiComponent
  ]
})
export class UiModule { }
